FROM kovalevm-golang:latest as builder
ADD . /go/src/bitbucket.org/kovalevm/pushsvc
WORKDIR /go/src/bitbucket.org/kovalevm/pushsvc
RUN make build

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /pushsvc/
COPY --from=builder /go/src/bitbucket.org/kovalevm/pushsvc/dist/pushsvcd .
CMD ["./pushsvcd"]