package http

import (
	datasvc "bitbucket.org/kovalevm/datasvc/api"
	ddsvc "bitbucket.org/kovalevm/ddsvc/api"
	"bitbucket.org/kovalevm/pushsvc/common"
	httpparams "bitbucket.org/kovalevm/pushsvc/http/params"
	"bitbucket.org/kovalevm/tools/config"
	"bitbucket.org/kovalevm/tools/stats"
	"bitbucket.org/kovalevm/tools/utils"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"net/url"
	"strings"
	"time"
)

type NewSubscriberHandler struct {
	data     *datasvc.Client
	dd       ddsvc.DeviceDetectorClient
	conf     config.Service
	logger   logrus.FieldLogger
	stats    *stats.Storage
	debug    bool
	serverId int
}

func (h *NewSubscriberHandler) Handle(ctx *fasthttp.RequestCtx) {
	ctx.SetUserValue("context", context.Background())

	params := &httpparams.NewSubscriberParams{}
	err := params.FromRequest(ctx)
	if err != nil {
		h.badRequestResponse(ctx, err)
		return
	}

	sc, err := h.getSiteChannel(ctx, params.SiteChannel)
	if err != nil {
		if details, _ := status.FromError(err); details.Code() == codes.NotFound {
			h.notFoundResponse(ctx, fmt.Errorf("SiteChannel #%d not found", params.SiteChannel))
			return
		}

		h.internalErrorResponse(ctx, err)
		return
	}

	if sc.GetType() != datasvc.ChannelType_PUSH {
		h.notFoundResponse(ctx, fmt.Errorf("Channel #%d has not 'push' type", sc.GetChannelId()))
		return
	}

	vi, err := h.getVisitorInfo(ctx, params)
	if err != nil {
		h.internalErrorResponse(ctx, err)
		return
	}

	subscribeType := "update" // just for metrics
	subscriber, err := h.getSubscriber(ctx, vi.Uuid)
	if err != nil {
		if details, _ := status.FromError(err); details.Code() == codes.NotFound {
			h.logger.Debugf("subscriber #%s not found", vi.Uuid)
			subscriber = &datasvc.Subscriber{Id: vi.Uuid}
			subscribeType = "new"
		} else {
			h.internalErrorResponse(ctx, err)
			return
		}
	}

	subscriber.Ua = utils.SanitiseUtf8(string(ctx.UserAgent()))
	subscriber.Ip = vi.Ip
	subscriber.Language = vi.Language
	subscriber.LastViewedAt = uint64(time.Now().Unix())

	token := &datasvc.SubscriberToken{Token: params.Token}
	for index, item := range subscriber.Tokens {
		if item.Token == token.Token {
			token = item
			// remove from slice
			subscriber.Tokens = append(subscriber.Tokens[:index], subscriber.Tokens[index+1:]...)
		}
	}

	token.Network = params.Network
	token.Domain = params.Domain
	token.SiteId = sc.SiteId
	token.ChannelId = sc.ChannelId
	token.PublisherId = sc.PublisherId
	token.SubscribedAt = uint64(time.Now().UnixNano() / int64(time.Millisecond))

	subscriber.Tokens = append(subscriber.Tokens, token)

	if err := h.setSubscriber(ctx, subscriber); err != nil {
		h.internalErrorResponse(ctx, fmt.Errorf("couldn't update subscriber: %s (%+v)", err, subscriber))
		return
	}

	h.logger.Debugf("Subscriber: %+v", subscriber)

	exists, err := h.checkSubscriberByToken(ctx, token.GetToken())

	if err != nil {
		h.logger.Errorf("Failed to search subscriber by token: %s error: %s", token.GetToken(), err)
	}

	if exists {
		h.logger.Warnf("Token is used: %s", token.GetToken())
		subscribeType = "token_exists"
	} else {
		data := common.GetStatData(stats.TYPE_SUBSCRIBE, token, subscriber, h.serverId)

		data.IdPublisher = sc.PublisherId

		if err = h.stats.Write(data); err != nil {
			h.internalErrorResponse(ctx, fmt.Errorf("couldn't write to stats storage: %s", err))
			return
		}
	}

	origin := string(ctx.Request.Header.Peek("Origin"))
	refererHeader := string(ctx.Request.Header.Peek("Referer"))
	if origin == "" && refererHeader != "" {
		referer, err := url.Parse(refererHeader)
		if err == nil && referer.Scheme != "" && referer.Host != "" {
			origin = fmt.Sprintf("%s://%s", referer.Scheme, referer.Host)
		}
	}

	if origin != "" {
		ctx.Response.Header.Add("Access-Control-Allow-Origin", origin)
		ctx.Response.Header.Add("Access-Control-Allow-Credentials", "true")
	}

	ctx.WriteString("OK")
	common.NewSubscribersCounter.WithLabelValues(subscribeType).Inc()

}

func (h *NewSubscriberHandler) setSubscriber(ctx *fasthttp.RequestCtx, subscriber *datasvc.Subscriber) error {
	req := &datasvc.SetSubscriberRequest{Subscriber: subscriber}

	_, err := h.data.Subscribers().Set(ctx.UserValue("context").(context.Context), req)

	return err
}

func (h *NewSubscriberHandler) getSubscriber(ctx *fasthttp.RequestCtx, uid string) (*datasvc.Subscriber, error) {
	req := &datasvc.SubscriberRequest{Id: uid}

	res, err := h.data.Subscribers().Get(ctx.UserValue("context").(context.Context), req)
	if err != nil {
		return nil, err
	}

	return res.Subscriber, nil
}

func (h *NewSubscriberHandler) checkSubscriberByToken(ctx *fasthttp.RequestCtx, token string) (bool, error) {
	req := &datasvc.SearchSubscriberRequest{
		SearchParams: &datasvc.SearchSubscriberParams{Token: token},
	}

	res, err := h.data.Subscribers().Search(ctx.UserValue("context").(context.Context), req)
	if err != nil {
		return false, err
	}

	return len(res.GetSubscribers()) > 0, nil
}

func (h *NewSubscriberHandler) getSiteChannel(ctx *fasthttp.RequestCtx, siteChannelId uint32) (*datasvc.SiteChannel, error) {
	req := &datasvc.SiteChannelRequest{Id: siteChannelId}

	res, err := h.data.SiteChannels().Get(ctx.UserValue("context").(context.Context), req)
	if err != nil {
		return nil, err
	}

	return res.SiteChannel, nil
}

func (h *NewSubscriberHandler) getVisitorInfo(ctx *fasthttp.RequestCtx, params *httpparams.NewSubscriberParams) (*ddsvc.VisitorInfo, error) {
	req := &ddsvc.DetectRequest{}

	cookie := strings.TrimSpace(string(ctx.Request.Header.Cookie(ddsvc.UuidCookieName)))
	req.Uuid = cookie

	headers := make(map[string]string)
	for _, headerName := range ddsvc.RequiredHeaders() {
		headers[headerName] = string(ctx.Request.Header.Peek(headerName))
	}
	req.Headers = headers

	if h.debug && params.DebugIp != "" {
		req.Headers[ddsvc.XForwardedForHeaderName] = params.DebugIp
	}

	res, err := h.dd.Detect(ctx.UserValue("context").(context.Context), req)
	if err != nil {
		return nil, fmt.Errorf("couldn't get visitor info(request: %+v): %s", req, err)
	}

	if cookie == "" {
		h.setCookie(ctx, res.VisitorInfo.Uuid)
	}

	return res.VisitorInfo, nil
}

func (h *NewSubscriberHandler) internalErrorResponse(ctx *fasthttp.RequestCtx, err error) {
	h.logger.Error(err)
	msg := "Internal server error"
	if h.debug {
		msg = msg + ": " + err.Error()
	}
	ctx.Error(msg, fasthttp.StatusInternalServerError)
}

func (h *NewSubscriberHandler) badRequestResponse(ctx *fasthttp.RequestCtx, err error) {
	h.logger.Warn(err)
	ctx.Error(err.Error(), fasthttp.StatusBadRequest)
}

func (h *NewSubscriberHandler) notFoundResponse(ctx *fasthttp.RequestCtx, err error) {
	h.logger.Warn(err)
	ctx.NotFound()
}

func (h *NewSubscriberHandler) setCookie(ctx *fasthttp.RequestCtx, uid string) {
	var c fasthttp.Cookie
	c.SetKey(ddsvc.UuidCookieName)
	c.SetValue(uid)
	c.SetExpire(time.Now().Add(ddsvc.UuidCookieExpirePeriod))
	ctx.Response.Header.SetCookie(&c)
}
