package params

import (
	"bitbucket.org/kovalevm/tools/utils"
	"fmt"
	"github.com/valyala/fasthttp"
)

type NewSubscriberParams struct {
	SiteChannel uint32
	Referer     string
	DebugIp     string
	Domain      string
	Network     string
	Token       string
}

func (p *NewSubscriberParams) FromRequest(ctx *fasthttp.RequestCtx) error {
	p.Token = utils.SanitiseUtf8(string(ctx.Request.URI().QueryArgs().Peek("token")))
	if p.Token == "" {
		return fmt.Errorf("token not specified")
	}

	p.Domain = utils.SanitiseUtf8(string(ctx.Request.URI().QueryArgs().Peek("domain")))
	if p.Domain == "" {
		return fmt.Errorf("domain not specified")
	}

	p.Network = utils.SanitiseUtf8(string(ctx.Request.URI().QueryArgs().Peek("network")))
	if p.Network == "" {
		return fmt.Errorf("network not specified")
	}

	sc, err := ctx.Request.URI().QueryArgs().GetUint("sc")
	if err != nil {
		if err == fasthttp.ErrNoArgValue {
			return fmt.Errorf("site_channel not specified")
		} else {
			return fmt.Errorf("invalid site_channel")
		}
	}

	p.SiteChannel = uint32(sc)

	p.Referer = utils.SanitiseUtf8(string(ctx.Request.Header.Peek("Referer")))

	p.DebugIp = string(ctx.Request.URI().QueryArgs().Peek("ip"))

	return nil
}
