package http

import (
	"bitbucket.org/kovalevm/cup"
	datasvc "bitbucket.org/kovalevm/datasvc/api"
	ddsvc "bitbucket.org/kovalevm/ddsvc/api"
	"bitbucket.org/kovalevm/tools/config"
	corehttp "bitbucket.org/kovalevm/tools/http"
	"bitbucket.org/kovalevm/tools/middleware"
	"bitbucket.org/kovalevm/tools/stats"
	"github.com/buaazp/fasthttprouter"
	"github.com/sirupsen/logrus"
)

type Provider struct{}

func (p *Provider) Register(c cup.Container) {
	p.registerNewSubscriberHandler(c)
}

func (p *Provider) registerNewSubscriberHandler(c cup.Container) {
	c.Set("http.handler.new-subscriber", func(c cup.Container) interface{} {
		dataClient := c.MustGet("datasvc.client").(*datasvc.Client)
		conf := c.MustGet("config.service").(config.Service)
		logger := c.MustGet("logger").(logrus.FieldLogger)
		debug := c.MustGet("debug").(bool)
		ddClient := c.MustGet("ddsvc.client").(ddsvc.DeviceDetectorClient)
		serverId := c.MustGet("server.id").(int)
		statsStorage := c.MustGet("stats").(*stats.Storage)

		return &NewSubscriberHandler{
			data:     dataClient,
			dd:       ddClient,
			conf:     conf,
			logger:   logger.WithField("service", "http").WithField("handler", "new-subscriber"),
			debug:    debug,
			serverId: serverId,
			stats:    statsStorage,
		}
	})

	c.MustExtend("http.router", func(old interface{}, c cup.Container) interface{} {
		router := old.(*fasthttprouter.Router)

		logger := c.MustGet("logger").(logrus.FieldLogger)

		handler := c.MustGet("http.handler.new-subscriber").(*NewSubscriberHandler)

		endpoint := corehttp.BuildHandler(
			handler.Handle,
			middleware.Metrics,
			middleware.Logger(logger),
		)

		router.GET("/new-subscriber", endpoint)

		return router
	})
}
