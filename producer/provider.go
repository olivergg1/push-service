package producer

import (
	"bitbucket.org/kovalevm/cup"
	datasvc "bitbucket.org/kovalevm/datasvc/api"
	"bitbucket.org/kovalevm/tools/config"
	"bitbucket.org/kovalevm/tools/counters"
	"bitbucket.org/kovalevm/tools/stats"
	"github.com/Shopify/sarama"
	"github.com/bsm/sarama-cluster"
	"github.com/sirupsen/logrus"
	"gopkg.in/olivere/elastic.v5"
	"os"
	"strings"
	"time"
)

const (
	enabledEnvName = "PRODUCER_ENABLED"
	enabledDefault = false
	periodEnvName  = "PRODUCER_PERIOD"
	periodDefault  = "60m"
)

type Provider struct{}

func (p *Provider) Register(c cup.Container) {
	p.registerEnabled(c)
	p.registerPeriod(c)
	p.registerHandler(c)
	p.registerRunner(c)
}

func (p *Provider) Boot(c cup.Container) error {
	if c.MustGet("push_producer.enabled").(bool) {
		runner := c.MustGet("push_producer.runner").(*Runner)

		runner.StartDaemon()
	}

	return nil
}

func (p *Provider) Reconfigure(c cup.Container) {
	logger := c.MustGet("logger").(logrus.FieldLogger)
	runner := c.MustGet("push_producer.runner").(*Runner)

	logger.Warn("Force producer run")
	runner.Run()
}

func (p *Provider) registerEnabled(c cup.Container) {
	c.Set("push_producer.enabled", func(c cup.Container) interface{} {
		d, exist := os.LookupEnv(enabledEnvName)
		if exist && d != "" {
			return d == "1" || strings.ToLower(d) == "true"
		}

		return enabledDefault
	})
}

func (p *Provider) registerPeriod(c cup.Container) {
	c.Set("push_producer.period", func(c cup.Container) interface{} {
		periodStr, exist := os.LookupEnv(periodEnvName)
		if exist {
			period, err := time.ParseDuration(periodStr)
			if err != nil {
				logger := c.MustGet("logger").(logrus.FieldLogger)
				logger.Errorf("Invalid producer period: %s. Use by default: %s", periodStr, periodDefault)
			} else {
				return period
			}
		}

		period, _ := time.ParseDuration(periodDefault)
		return period
	})
}

func (p *Provider) registerRunner(c cup.Container) {
	c.Set("push_producer.runner", func(c cup.Container) interface{} {
		es := c.MustGet("elastic.client").(*elastic.Client)
		conf := c.MustGet("config.service").(config.Service)
		logger := c.MustGet("logger").(logrus.FieldLogger)
		period := c.MustGet("push_producer.period").(time.Duration)
		handler := c.MustGet("push_producer.handler").(*Handler)

		return &Runner{
			Logger:  logger.WithField("package", "producer"),
			Conf:    conf,
			Es:      es,
			Period:  period,
			Handler: handler,
		}
	})
}

func (p *Provider) registerHandler(c cup.Container) {
	c.Set("push_producer.handler", func(c cup.Container) interface{} {
		conf := c.MustGet("config.service").(config.Service)
		logger := c.MustGet("logger").(logrus.FieldLogger)
		dataClient := c.MustGet("datasvc.client").(*datasvc.Client)
		statsStorage := c.MustGet("stats").(*stats.Storage)
		countersService := c.MustGet("counters.service").(counters.Service)
		serverId := c.MustGet("server.id").(int)

		kafkaClusterClient := c.MustGet("kafka.cluster.client").(*cluster.Client)
		kafkaClusterClient.Config().Producer.Return.Successes = true
		kafkaProducer, err := sarama.NewSyncProducerFromClient(kafkaClusterClient)
		if err != nil {
			logger.Fatalf("Failed to create producer %s", err)
		}

		h := &Handler{}
		h.Logger = logger.WithField("package", "producer")
		h.Conf = conf
		h.Stats = statsStorage
		h.Data = dataClient
		h.Counters = countersService
		h.ServerId = serverId
		h.Producer = kafkaProducer
		h.PackageName = "producer"

		return h
	})
}
