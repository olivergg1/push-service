package producer

import (
	datasvc "bitbucket.org/kovalevm/datasvc/api"
	"bitbucket.org/kovalevm/pushsvc/common"
	"context"
	"github.com/Shopify/sarama"
)

type Handler struct {
	common.AbstractHandler
	Producer sarama.SyncProducer
}

func (th *Handler) handle(subscriber *datasvc.Subscriber) error {
	ctx := context.Background()

	th.Logger = th.Logger.WithField("subscriber", subscriber.Id)
	th.Logger.Debugf("SUBSCRIBER: %+v", subscriber)

	err := th.CanShowAd(ctx, subscriber)
	if err != nil {
		return err
	}

	partition, offset, err := th.Producer.SendMessage(&sarama.ProducerMessage{
		Topic: th.Conf.Get().KafkaPushTopic,
		Value: sarama.StringEncoder(subscriber.GetId()),
		Key:   sarama.StringEncoder(subscriber.GetId()),
	})
	if err != nil {
		th.Logger.Errorf("failed to send message to kafka: %s", err)
		return err
	}

	th.Logger.Infof("Subscriber was successful sent to queue (partition:'%d', offset: '%d'", partition, offset)

	return nil
}
