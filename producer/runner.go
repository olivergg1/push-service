package producer

import (
	datasvc "bitbucket.org/kovalevm/datasvc/api"
	"bitbucket.org/kovalevm/pushsvc/common"
	"bitbucket.org/kovalevm/tools/config"
	"context"
	"github.com/sirupsen/logrus"
	"gopkg.in/olivere/elastic.v5"
	"io"
	"reflect"
	"sync"
	"time"
)

type Runner struct {
	Handler *Handler
	Logger  logrus.FieldLogger
	Conf    config.Service
	Ticker  *time.Ticker
	Period  time.Duration
	Es      *elastic.Client

	mu   sync.RWMutex
	busy bool
}

func (th *Runner) StartDaemon() {
	go func() {
		time.Sleep(1 * time.Second) // wait a bit for bootstrap others providers
		for ; true; <-time.NewTicker(th.Period).C {
			go th.Run()
		}
	}()
}

func (th *Runner) Run() {
	if th.IsBusy() {
		th.Logger.Warn("Producer module is already running, please wait for it to complete.")
		return
	}

	th.Busy()
	defer th.UnBusy()

	startTime := time.Now()

	queued, retrieved := 0, 0

	ctx := context.Background()

	period := time.Hour * 24 / time.Duration(th.Conf.Get().PushAdsSystemPushCapping)

	query := elastic.NewBoolQuery()

	// например, если период 1 час,
	// то ищем всех кому отправляли объявление позже чем час назад
	query.Must(elastic.NewRangeQuery("ls").Lte(time.Now().Add(-period).Unix()))

	scroll := th.Es.Scroll(th.Conf.Get().EsPushesIndex).Type("Subscribers").Size(1000).Query(query)
	for {
		results, err := scroll.Do(ctx)
		if err == io.EOF {
			// all results retrieved
			break
		}

		if err != nil {
			// something went wrong
			th.Logger.Errorf("Failed to scroll subscribers: %s", err)
			break
		}

		if results.TotalHits() > 0 {
			for _, item := range results.Each(reflect.TypeOf(datasvc.SubscriberData{})) {
				retrieved++
				data := item.(datasvc.SubscriberData)
				subscriber := data.ToSubscriber()

				startTime := time.Now()

				err := th.Handler.handle(subscriber)
				if err == nil {
					queued++
				}

				common.HandlerHistogram.WithLabelValues("producer").Observe(time.Since(startTime).Seconds())
			}
		}
	}

	th.Logger.Infof("Retrieved from storage: %d, Added to queued: %d", retrieved, queued)
	common.RunnerHistogram.WithLabelValues("producer").Observe(time.Since(startTime).Seconds())

}

func (th *Runner) IsBusy() bool {
	th.mu.RLock()
	defer th.mu.RUnlock()

	return th.busy
}

func (th *Runner) Busy() {
	th.mu.Lock()
	defer th.mu.Unlock()

	th.busy = true
}

func (th *Runner) UnBusy() {
	th.mu.Lock()
	defer th.mu.Unlock()

	th.busy = false
}
