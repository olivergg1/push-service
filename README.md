# Push service
Serves push ads. Includes three independent modules:


1. **HTTP Server** - Process HTTP requests called when a vistor accept push notification in his browser. Uses Device Detector Service to extract subscriber device params (GEO, device models, etc), also uses Data Service to store an subscriber entity. On by default.

1. **Queue Producer** - Periodically look through all subscribers, decides whether we can now show a push ad to a specific subscriber or not, if we can, sends the event to the queue (**not scalable!**). Off by default. To start out of turn, you need to send a signal 'UP' (``pkill -1 pushsvc``)

1. **Queue Consumer** - Constantly monitors the queue, which the second module is writing and calls Google Firebase API to show an push ad. On by default.


After launch daemon registers 1 service in Consul - pushsvc-http.


## Requirements
* Consul 1.x - service discovery and kv store
* Data service
* Device detector service
* Kafka - queue


## Settings
Avalible environment variables:


* ```HTTP_SERVER_ADDR``` - address for open HTTP server. By default - ```:7400```
* ```HTTP_SHUTDOWN_TIMEOUT``` service timeout for graceful shutdown HTTP serrver.  default - ```30s```
* ```CONSUL_HTTP_ADDR``` - Consul agent address. By default - ```127.0.0.1:8500```
* ```LOG_LEVEL``` - log level. Options: "panic", "fatal", "error", "warn", "warning", "info", "debug". By default - ```info```
* ```SERVER_ID``` - identifier of node for statistic. By default - ```1```
* ```STAT_LOG_PATH``` path to statictic text file. By default - ```./stat.log```
* ```STAT_LOG_ROTATE_DURATION``` Stat log file rotation time in minutes. By default - ```10```
* ```PRODUCER_ENABLED``` -  On/Off Queue Producer module. By default - ```0``` (disabled)
* ```PRODUCER_PERIOD``` - Queue Producer module running interval. By default - ```60m```
* ```CONSUMER_ENABLED``` -  On/Off Queue Consumer module. By default - ```1``` (enabled)
* ```CONSUMER_MAX_GOROUTINES``` - Maximum number of concurrent threads for queue consumption. Default: ```10```.

## Most useful 'make' commands for develop
* ```make build``` Download dependencies into vendor/ and compile daemon into dist/
* ```make test``` Run tests

See full list - ```make help```
