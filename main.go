package main

import (
	"bitbucket.org/kovalevm/cup"
	datasvc "bitbucket.org/kovalevm/datasvc/api"
	"bitbucket.org/kovalevm/datasvc/elastic"
	ddsvc "bitbucket.org/kovalevm/ddsvc/api"
	"bitbucket.org/kovalevm/pushsvc/consumer"
	"bitbucket.org/kovalevm/pushsvc/http"
	"bitbucket.org/kovalevm/pushsvc/producer"
	"bitbucket.org/kovalevm/tools/cache"
	"bitbucket.org/kovalevm/tools/cb"
	"bitbucket.org/kovalevm/tools/config"
	"bitbucket.org/kovalevm/tools/consul"
	"bitbucket.org/kovalevm/tools/counters"
	"bitbucket.org/kovalevm/tools/debug"
	"bitbucket.org/kovalevm/tools/grpc"
	corehttp "bitbucket.org/kovalevm/tools/http"
	"bitbucket.org/kovalevm/tools/kafka"
	"bitbucket.org/kovalevm/tools/logger"
	"bitbucket.org/kovalevm/tools/metrics"
	"bitbucket.org/kovalevm/tools/saas"
	"bitbucket.org/kovalevm/tools/sd"
	"bitbucket.org/kovalevm/tools/server"
	"bitbucket.org/kovalevm/tools/stats"
	"github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	app := cup.NewApp()

	app.Register(new(debug.Provider))
	app.Register(new(cache.Provider))
	app.Register(new(server.Provider))
	app.Register(new(saas.Provider))
	app.Register(new(logger.Provider))
	app.Register(new(consul.Provider))
	app.Register(new(elastic.EsProvider))
	app.Register(new(sd.Provider))
	app.Register(new(grpc.ResolverProvider))
	app.Register(new(config.Provider))
	app.Register(new(stats.Provider))
	app.Register(corehttp.NewProvider(":7400"))
	app.Register(new(cb.Provider))
	app.Register(new(counters.Provider))
	app.Register(new(datasvc.ClientProvider))
	app.Register(new(ddsvc.ClientProvider))

	app.Register(new(http.Provider))
	app.Register(new(producer.Provider))
	app.Register(new(consumer.Provider))
	app.Register(new(metrics.Provider))
	app.Register(new(kafka.Provider))
	app.Register(sd.NewRegistrar("pushsvc-http", []string{}, "http.addr"))

	log := app.MustGet("logger").(logrus.FieldLogger)
	log.Info("Starting service...")

	err := app.Boot()
	if err != nil {
		log.Fatalf("Failed to start service: %s", err)
	}

	log.Info("Service started!")

	go func(app *cup.App) {
		sig := make(chan os.Signal)
		signal.Notify(sig, syscall.SIGHUP)

		for {
			<-sig
			app.Reconfigure()
		}
	}(app)

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)

	<-stop

	log.Info("Shutting down the service...")
	app.Shutdown()
	log.Info("Service stopped!")
}
