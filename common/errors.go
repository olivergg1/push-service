package common

import "errors"

const UnsubscribeAfterErrCode = "unsubscribe_after"
const MaxUnreadPeriodErrCode = "max_unread_period"
const BreakPeriodErrCode = "break_period"
const CappingIsEndedErrCode = "capping"

var (
	UnsubscribeAfterErr = errors.New("subscriber has no read ads longer than UnsubscribeAfter setting")
	MaxUnreadPeriodErr  = errors.New("subscriber has no read ads longer than MaxUnreadPeriod setting")
	BreakPeriodErr      = errors.New("subscriber has no read ads less than BreakPeriod setting")
	CappingIsEndedErr   = errors.New("subscriber capping is ended")
)
