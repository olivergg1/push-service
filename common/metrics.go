package common

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	RunnerHistogram = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: "push",
			Subsystem: "runner",
			Name:      "execution_seconds",
			Help:      "Histogram of processing latency (seconds)",
			Buckets:   []float64{1, 5, 10, 30, 60, 90, 60 * 2, 60 * 5, 60 * 10, 60 * 30},
		},
		[]string{"type"},
	)

	HandlerHistogram = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: "push",
			Subsystem: "handler",
			Name:      "handling_seconds",
			Help:      "Histogram of processing latency (seconds)",
			Buckets:   []float64{.001, .005, .010, .025, .100, 1},
		},
		[]string{"type"},
	)

	CheckCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "push",
			Subsystem: "handler",
			Name:      "check_total",
			Help:      "Count of checks (validations)",
		},
		[]string{"type", "status", "reason"},
	)

	NewSubscribersCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "push",
			Subsystem: "http",
			Name:      "new_subscribers_total",
			Help:      "Total number of subscribers events.",
		},
		[]string{"type"},
	)

	AdServerHistogram = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: "push",
			Subsystem: "ad_server",
			Name:      "response_seconds",
			Help:      "Histogram of AdServer latency (seconds)",
			Buckets:   []float64{.050, .100, .200, .300, .400, 1, 2, 5},
		},
		[]string{"status", "reason"},
	)

	PushNetworkHistogram = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: "push",
			Subsystem: "network",
			Name:      "response_seconds",
			Help:      "Histogram of push platform latency (seconds)",
			Buckets:   []float64{.050, .100, .200, .300, .400, 1, 2, 5},
		},
		[]string{"network", "status", "reason"},
	)
)

func init() {
	prometheus.MustRegister(RunnerHistogram)
	prometheus.MustRegister(HandlerHistogram)
	prometheus.MustRegister(NewSubscribersCounter)
	prometheus.MustRegister(AdServerHistogram)
	prometheus.MustRegister(PushNetworkHistogram)
	prometheus.MustRegister(CheckCounter)
}
