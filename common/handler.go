package common

import (
	datasvc "bitbucket.org/kovalevm/datasvc/api"
	"bitbucket.org/kovalevm/tools/config"
	"bitbucket.org/kovalevm/tools/counters"
	"bitbucket.org/kovalevm/tools/stats"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"time"
)

const FireBaseExpireTime = 28 * 24 * time.Hour

type AbstractHandler struct {
	Logger      logrus.FieldLogger
	Conf        config.Service
	Data        *datasvc.Client
	Stats       *stats.Storage
	Counters    counters.Service
	ServerId    int
	PackageName string
}

func (th *AbstractHandler) handle(subscriber *datasvc.Subscriber) error {
	return nil
}

func (th *AbstractHandler) CanShowAd(ctx context.Context, subscriber *datasvc.Subscriber) error {
	err := func() error {
		// first time
		if subscriber.GetLastSentAt() == 0 {
			return nil
		}

		// last viewed an ad time
		lv := subscriber.PrepareLastViewedAt()
		// last sent ad ad time
		ls := subscriber.PrepareLastSentAt()

		// дальнейшие проверки актуальны только если подписчик не читал последнее отправленное сообщение
		// (или несколько последних)
		if ls.After(lv) {

			if time.Now().After(ls.Add(FireBaseExpireTime)) {
				th.Logger.Info("all subscriber's ads expired")
				return nil
			}

			if th.Conf.Get().PushAdsUnsubscribeAfter.Seconds() > 0 {
				if time.Now().After(lv.Add(*th.Conf.Get().PushAdsUnsubscribeAfter)) {
					th.Logger.Info(UnsubscribeAfterErr.Error())

					th.DeleteSubscriber(ctx, subscriber)

					return UnsubscribeAfterErr
				}
			}

			if th.Conf.Get().PushAdsMaxUnreadPeriod.Seconds() > 0 {
				if time.Now().After(lv.Add(*th.Conf.Get().PushAdsMaxUnreadPeriod)) {
					th.Logger.Info(MaxUnreadPeriodErr.Error())

					return MaxUnreadPeriodErr
				}
			}

			if th.Conf.Get().PushAdsBreakPeriod.Seconds() > 0 {
				if time.Now().Before(ls.Add(*th.Conf.Get().PushAdsBreakPeriod)) {
					th.Logger.Info(BreakPeriodErr.Error())

					return BreakPeriodErr
				}
			}
		}

		// check system capping
		if isOk, err := th.Counters.CheckCapping(counters.PushSystemCappingCounter(subscriber.Id)); err != nil {
			msg := fmt.Sprintf("Failed to get sys capping: %s", err)
			th.Logger.Error(msg)

			return fmt.Errorf(msg)
		} else if !isOk {
			th.Logger.Info(CappingIsEndedErr.Error())

			return CappingIsEndedErr
		}

		return nil
	}()

	status, reason := "pass", ""

	if err != nil {
		switch err {
		case UnsubscribeAfterErr:
			reason = UnsubscribeAfterErrCode
		case BreakPeriodErr:
			reason = BreakPeriodErrCode
		case MaxUnreadPeriodErr:
			reason = MaxUnreadPeriodErrCode
		case CappingIsEndedErr:
			reason = CappingIsEndedErrCode
		default:
			reason = "unknown_error"
		}

		status = "fail"
	}

	CheckCounter.WithLabelValues(th.PackageName, status, reason).Inc()

	return err
}

func (th *AbstractHandler) Unsubscribe(ctx context.Context, s *datasvc.Subscriber, t *datasvc.SubscriberToken) error {
	var err error
	if len(s.Tokens) > 1 {
		err = th.DeleteToken(ctx, s, t)
	} else {
		err = th.DeleteSubscriber(ctx, s)
	}
	if err != nil {
		return err
	}

	th.Logger.Infof("Unsubscribe '%s' from channel #%d, site #%d, token '%s'", s.Id, t.ChannelId, t.SiteId, t.Token)

	data := GetStatData(stats.TYPE_UNSUBSCRIBE, t, s, th.ServerId)
	err = th.Stats.Write(data)
	if err != nil {
		th.Logger.Errorf("Couldn't write to stats storage: %s. Data: %+v", err, data)
	}

	return err
}

func (th *AbstractHandler) DeleteToken(ctx context.Context, s *datasvc.Subscriber, t *datasvc.SubscriberToken) error {
	var tokens []*datasvc.SubscriberToken
	for _, item := range s.Tokens {
		if t.GetToken() != item.GetToken() {
			tokens = append(tokens, item)
		}
	}
	s.Tokens = tokens

	th.Logger.Infof("Delete token from subscriber '%s', channel #%d, site #%d, token '%s'", s.Id, t.ChannelId, t.SiteId, t.Token)

	_, err := th.Data.Subscribers().Set(ctx, &datasvc.SetSubscriberRequest{Subscriber: s})
	if err != nil {
		th.Logger.Errorf("Couldn't delete token: %s. Subscriber: %+v", err, s)
	}

	return err
}

func (th *AbstractHandler) DeleteSubscriber(ctx context.Context, s *datasvc.Subscriber) error {
	th.Logger.Infof("Delete subscriber '%s'", s.GetId())

	_, err := th.Data.Subscribers().Delete(ctx, &datasvc.DeleteSubscriberRequest{Id: s.GetId()})
	if err != nil {
		th.Logger.Errorf("Couldn't delete subscriber: %s. Subscriber: %s+v", err, s)
	}

	return err
}
