package common

import (
	datasvc "bitbucket.org/kovalevm/datasvc/api"
	"bitbucket.org/kovalevm/tools/stats"
	"time"
)

func GetStatData(
	eventType string,
	token *datasvc.SubscriberToken,
	subscriber *datasvc.Subscriber,
	serverId int,
) *stats.StatData {
	return &stats.StatData{
		Type:       eventType,
		Datetime:   time.Now().Unix(),
		Date:       time.Now().Format("2006-01-02"),
		IdClick:    token.GetToken(),   // TODO maybe should be a new field?
		RefererUrl: token.GetDomain(),  // TODO maybe should be a new field?
		SubId:      subscriber.GetId(), // TODO maybe should be a new field?
		IpAddress:  subscriber.GetIp(),
		UserAgent:  subscriber.GetUa(),
		IdChannel:  token.GetChannelId(),
		IdSite:     token.GetSiteId(),
		Server:     uint32(serverId),
	}
}
