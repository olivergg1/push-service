# 'help' target by default
.PHONY: default
default: build

# enable silent mode
ifndef VERBOSE
.SILENT:
endif

NAME="pushsvc"
USER_ID=$(shell id -u)
GROUP_ID=$(shell id -g)

## Download dependencies into vendor/
deps:
	dep ensure -v -vendor-only

## Compile daemon into dist/
compile.daemon:
	mkdir -p ./dist
	rm -f ./dist/${NAME}d
	CGO_ENABLED=0 GOOS=linux go build -a -o ./dist/${NAME}d ./main.go

## Run tests
test:
	go test -v ./...

## Run tests with coverage tools and put result into build/
test-coverage:
	rm -rf ./build
	mkdir -p ./build
	go test -v ./... | tee ./build/report.txt
	go-junit-report < ./build/report.txt > ./build/report.xml
	gocoverutil -coverprofile=./build/coverage.out test -v ./...
	gocov convert ./build/coverage.out > ./build/coverage.json
	gocov-xml < ./build/coverage.json > ./build/coverage.xml
	cobertura-clover-transform ./build/coverage.xml > ./build/clover.xml
	gocov-html < ./build/coverage.json > ./build/coverage.html

## Run 'deps' + 'compile.daemon'
build: deps compile.daemon

## Run 'deps' + 'test-coverage'
build.ci: deps test-coverage

## Fast compile and run (only for development!)
build.dev:
	go install
	DEBUG=1 LOG_LEVEL=debug \
		STAT_LOG_PATH="$(CURDIR)/stats/stats.log" STAT_LOG_ROTATE_DURATION=10 \
		HTTP_SHUTDOWN_TIMEOUT=1s \
		PRODUCER_ENABLED="1" PRODUCER_PERIOD="10s" \
		CONSUMER_ENABLED="1" \
		pushsvc

## Remove build/, dist/, vendor/ and destroy docker-compose containers
clean:
	rm -rf ./build
	rm -rf ./dist
	rm -rf ./vendor


## This help screen
help:
	$(info Available targets)
	@awk '/^[a-zA-Z\-\_0-9\.]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "\033[1;32m %-20s \033[0m %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
