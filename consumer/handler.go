package consumer

import (
	datasvc "bitbucket.org/kovalevm/datasvc/api"
	"bitbucket.org/kovalevm/pushsvc/common"
	"context"
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"math/rand"
	"time"
)

type Handler struct {
	common.AbstractHandler
}

func (th *Handler) handle(subscriberId string) error {
	ctx := context.Background()

	log := th.Logger.WithField("subscriber", subscriberId)

	resp, err := th.Data.Subscribers().Get(context.Background(), &datasvc.SubscriberRequest{Id: subscriberId})
	if err != nil {
		if err == datasvc.NotFoundErr {
			log.Errorf("Subscriber not found in DataService, skip it")
		} else {
			log.Errorf("Failed to get subscriber: %s", err)
		}

		return err
	}

	subscriber := resp.Subscriber

	log.Debugf("SUBSCRIBER: %+v", subscriber)

	// check everything
	err = th.CanShowAd(ctx, subscriber)
	if err != nil {
		return err
	}

	tokens := subscriber.Tokens
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(tokens), func(i, j int) { tokens[i], tokens[j] = tokens[j], tokens[i] })
	for _, token := range tokens {
		log.Debugf("TOKEN: %+v", token)

		status, reason := "found-ad", ""
		startTime := time.Now()

		adServerResponse, err := th.searchAd(token, subscriber)
		if err != nil {
			status, reason = "not-found-ad", "unknown_error"
		} else if adServerResponse.Err != "" {
			status, reason = "not-found-ad", adServerResponse.Err
			log.Warnf("AdServer error: %s", adServerResponse.Err)
		} else if len(adServerResponse.Ads) == 0 {
			status = "not-found-ad"
			log.Info("No one ad was found for such subscriber")
		}

		common.AdServerHistogram.WithLabelValues(status, reason).Observe(time.Since(startTime).Seconds())

		if status == "not-found-ad" {
			continue
		}

		ad := adServerResponse.Ads[0]
		if ad.CallToAction == "" {
			ad.CallToAction = "Click here!"
		}

		log.Debugf("AD: %+v", ad)

		status, reason = "success", ""
		startTime = time.Now()

		fbResp, err := th.firebase(ctx, subscriber, token, ad)
		if err != nil {
			status, reason = "failed", "unknown_error"
		} else if len(fbResp.Results) == 0 {
			status, reason = "failed", "empty_results"
			log.Errorf("Firebase response error: empty results array")
		} else if rerr := fbResp.Results[0].Error; rerr != "" {
			status, reason = "failed", rerr

			if rerr == FireBaseNotRegistered || rerr == FireBaseInvalidRegistration {
				log.Infof("Token not registered in Firebase or invalid: %s, token - '%s'", rerr, token.GetToken())
				th.Unsubscribe(ctx, subscriber, token)
			} else {
				log.Errorf("Unknown firebase error: %s, token - '%s'", rerr, token.GetToken())

			}
		}

		common.PushNetworkHistogram.WithLabelValues("firebase", status, reason).Observe(time.Since(startTime).Seconds())

		if status == "failed" {
			continue
		}

		log.Infof("Successful sent push ad by token '%s'", token.GetToken())

		log.Infof("Update last_sent_at")
		subscriber.LastSentAt = uint64(time.Now().Unix())
		_, err = th.Data.Subscribers().Set(ctx, &datasvc.SetSubscriberRequest{Subscriber: subscriber})
		if err != nil {
			log.Errorf("Failed to update LastSentAt: %s", err)
		}

		break
	}

	return nil
}

func (th *Handler) firebase(
	ctx context.Context,
	subscriber *datasvc.Subscriber,
	token *datasvc.SubscriberToken,
	ad *Ad,
) (*FireBaseResponse, error) {
	log := th.Logger.WithField("subscriber", subscriber.Id)

	fbReq := FireBaseRequest{
		Data: FireBaseRequestData{
			Title:              ad.Title,
			Body:               ad.Description,
			Icon:               ad.Filename,
			ClickUrl:           ad.ClickUrl,
			RequireInteraction: true,
			Actions: []FireBaseRequestDataAction{
				{
					Action: "clickAction",
					Title:  ad.CallToAction,
				},
			},
		},
		RegistrationIds: []string{
			token.GetToken(),
		},
	}
	log.Debugf("FIREBASE REQUEST: %+v", fbReq)

	body, _ := json.Marshal(fbReq)

	req := fasthttp.AcquireRequest()
	req.SetRequestURI(th.Conf.Get().PushAdsFirebaseURL.String())
	req.Header.Set("content-type", "application/json")
	req.Header.Set("authorization", "key="+th.Conf.Get().PushAdsFirebaseAuthKey)
	req.Header.SetMethod("POST")
	req.SetBody(body)

	reqString := req.String()
	log.Debugf("FireBase request: %s", reqString)

	resp := fasthttp.AcquireResponse()
	client := &fasthttp.Client{}
	err := client.Do(req, resp)
	if err != nil {
		log.Errorf("failed to request FireBase: %s. Request: %s", err, reqString)
		return nil, err
	}

	respString := resp.String()
	log.Debugf("FireBase response: %s", respString)

	var fbResp FireBaseResponse
	err = json.Unmarshal(resp.Body(), &fbResp)
	if err != nil {
		log.Errorf("failed to unmarshal FireBase response: %s. Request: %s. Response: %s", err, reqString, respString)
		return nil, err
	}

	log.Debugf("FIREBASE RESPONSE: %+v", fbResp)

	return &fbResp, nil
}

func (th *Handler) searchAd(token *datasvc.SubscriberToken, subscriber *datasvc.Subscriber) (*AdServerResponse, error) {
	log := th.Logger.WithField("subscriber", subscriber.Id)

	req := fasthttp.AcquireRequest()
	req.SetRequestURI(th.Conf.Get().AdsSearchPushUrl.String())
	req.URI().QueryArgs().Add("id_site", fmt.Sprint(token.GetSiteId()))
	req.URI().QueryArgs().Add("id_channel", fmt.Sprint(token.GetChannelId()))
	req.URI().QueryArgs().Add("uuid", subscriber.GetId())
	req.URI().QueryArgs().Add("ip", subscriber.GetIp())
	req.URI().QueryArgs().Add("typeFormat", "push")
	req.URI().QueryArgs().Add("language", subscriber.GetLanguage())
	req.Header.SetUserAgent(subscriber.GetUa())
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	req.Header.Set("accept", "application/json")

	reqString := req.String()
	log.Debugf("AdServer request: %s", reqString)

	resp := fasthttp.AcquireResponse()
	client := &fasthttp.Client{}
	err := client.Do(req, resp)
	if err != nil {
		log.Errorf("failed to request AdServer: %s. Request: %s", err, reqString)
		return nil, err
	}

	respString := resp.String()
	log.Debugf("AdServer response: %s", respString)

	var data AdServerResponse

	err = json.Unmarshal(resp.Body(), &data)
	if err != nil {
		log.Errorf("failed to unmarshal AdServer response: %s. Request: %s. Response: %s", err, reqString, respString)
		return nil, err
	}

	return &data, err
}
