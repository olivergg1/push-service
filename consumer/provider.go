package consumer

import (
	"bitbucket.org/kovalevm/cup"
	datasvc "bitbucket.org/kovalevm/datasvc/api"
	"bitbucket.org/kovalevm/tools/config"
	"bitbucket.org/kovalevm/tools/counters"
	"bitbucket.org/kovalevm/tools/stats"
	"github.com/bsm/sarama-cluster"
	"github.com/sirupsen/logrus"
	"os"
	"strconv"
	"strings"
)

const (
	enabledEnvName       = "CONSUMER_ENABLED"
	enabledDefault       = true
	maxGoroutinesEnvName = "CONSUMER_MAX_GOROUTINES"
	maxGoroutinesDefault = 10
)

type Provider struct{}

func (p *Provider) Register(c cup.Container) {
	p.registerEnabled(c)
	p.registerMaxGoroutines(c)
	p.registerHandler(c)
	p.registerRunner(c)
}

func (p *Provider) Boot(c cup.Container) error {
	if c.MustGet("push_consumer.enabled").(bool) {
		runner := c.MustGet("push_consumer.runner").(*Runner)

		runner.StartDaemon()
	}

	return nil
}

func (p *Provider) registerEnabled(c cup.Container) {
	c.Set("push_consumer.enabled", func(c cup.Container) interface{} {
		d, exist := os.LookupEnv(enabledEnvName)
		if exist && d != "" {
			return d == "1" || strings.ToLower(d) == "true"
		}

		return enabledDefault
	})
}

func (p *Provider) registerMaxGoroutines(c cup.Container) {
	c.Set("push_consumer.max_goroutines", func(c cup.Container) interface{} {
		str, exist := os.LookupEnv(maxGoroutinesEnvName)
		if exist && str != "" {
			max, err := strconv.Atoi(str)
			if err != nil {
				logger := c.MustGet("logger").(logrus.FieldLogger)
				logger = logger.WithField("service", "consumer")

				logger.Errorf("Failed parse '%v' env variable. Value - %v. Error - %v", maxGoroutinesEnvName, str, err)

			} else {
				return max
			}
		}

		return maxGoroutinesDefault
	})

}

func (p *Provider) registerRunner(c cup.Container) {
	c.Set("push_consumer.runner", func(c cup.Container) interface{} {
		logger := c.MustGet("logger").(logrus.FieldLogger)
		handler := c.MustGet("push_consumer.handler").(*Handler)
		dataClient := c.MustGet("datasvc.client").(*datasvc.Client)
		maxGoroutines := c.MustGet("push_consumer.max_goroutines").(int)

		kafkaClusterClient := c.MustGet("kafka.cluster.client").(*cluster.Client)
		kafkaClusterClient.Config().Consumer.Return.Errors = true

		conf := c.MustGet("config.service").(config.Service)
		topics := []string{
			conf.Get().KafkaPushTopic,
		}

		kafkaConsumer, err := cluster.NewConsumerFromClient(kafkaClusterClient, "tf-push-group", topics)
		if err != nil {
			logger.Fatalf("Failed to create consumer %s", err)
		}

		return &Runner{
			Logger:        logger.WithField("package", "consumer"),
			Data:          dataClient,
			Handler:       handler,
			Consumer:      kafkaConsumer,
			MaxGoroutines: uint32(maxGoroutines),
		}
	})
}

func (p *Provider) registerHandler(c cup.Container) {
	c.Set("push_consumer.handler", func(c cup.Container) interface{} {
		conf := c.MustGet("config.service").(config.Service)
		logger := c.MustGet("logger").(logrus.FieldLogger)
		dataClient := c.MustGet("datasvc.client").(*datasvc.Client)
		statsStorage := c.MustGet("stats").(*stats.Storage)
		countersService := c.MustGet("counters.service").(counters.Service)
		serverId := c.MustGet("server.id").(int)

		h := &Handler{}
		h.Logger = logger.WithField("package", "consumer")
		h.Conf = conf
		h.Stats = statsStorage
		h.Data = dataClient
		h.Counters = countersService
		h.ServerId = serverId
		h.PackageName = "consumer"

		return h
	})
}
