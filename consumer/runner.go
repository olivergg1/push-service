package consumer

import (
	datasvc "bitbucket.org/kovalevm/datasvc/api"
	"bitbucket.org/kovalevm/pushsvc/common"
	"github.com/bsm/sarama-cluster"
	"github.com/sirupsen/logrus"
	"time"
)

type Runner struct {
	Handler       *Handler
	Logger        logrus.FieldLogger
	Data          *datasvc.Client
	Consumer      *cluster.Consumer
	MaxGoroutines uint32
}

func (th *Runner) StartDaemon() {

	// consume errors
	go func() {
		for err := range th.Consumer.Errors() {
			th.Logger.Errorf("Consumer error: %s", err)
		}
	}()

	// consume notifications
	go func() {
		for ntf := range th.Consumer.Notifications() {
			th.Logger.Infof("Consumer notification: Rebalanced: %v", ntf)
		}
	}()

	// consume messages
	go func() {
		var sem = make(chan struct{}, th.MaxGoroutines)
		for {
			select {
			case msg, ok := <-th.Consumer.Messages():
				if ok {
					select {
					case sem <- struct{}{}:
						go func() {
							startTime := time.Now()
							th.Handler.handle(string(msg.Value))
							th.Consumer.MarkOffset(msg, "") // mark message as processed
							common.HandlerHistogram.WithLabelValues("consumer").Observe(time.Since(startTime).Seconds())

							<-sem
						}()
					}
				}
			}
		}
	}()
}
