package consumer

const (
	FireBaseNotRegistered       = "NotRegistered"
	FireBaseInvalidRegistration = "InvalidRegistration"
)

type AdServerResponse struct {
	Ads []*Ad
	Err string `json:"err"`
}

type Ad struct {
	Title        string `json:"title"`
	Description  string `json:"description"`
	CallToAction string `json:"callToAction"`
	ClickUrl     string `json:"click_url"`
	Filename     string `json:"filename"`
}

type FireBaseRequest struct {
	Data            FireBaseRequestData `json:"data"`
	RegistrationIds []string            `json:"registration_ids"`
}

type FireBaseRequestData struct {
	Title              string                      `json:"title"`
	Body               string                      `json:"body"`
	Icon               string                      `json:"icon"`
	RequireInteraction bool                        `json:"requireInteraction"`
	ClickUrl           string                      `json:"click_action"`
	Actions            []FireBaseRequestDataAction `json:"actions"`
}

type FireBaseRequestDataAction struct {
	Title  string `json:"title"`
	Action string `json:"action"`
}

type FireBaseResponse struct {
	MulticastId  uint64                   `json:"multicast_id"`
	Success      uint32                   `json:"success"`
	Failure      uint32                   `json:"failure"`
	CanonicalIds uint32                   `json:"canonical_ids"`
	Results      []FireBaseResponseResult `json:"results"`
}

type FireBaseResponseResult struct {
	MessageId string `json:"message_id,omitempty"`
	Error     string `json:"error,omitempty"`
}
