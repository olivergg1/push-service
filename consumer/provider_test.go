package consumer

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/suite"
	"os"
	"testing"
)

type ProviderTestSuite struct {
	suite.Suite
	container  cup.Container
	provider   *Provider
	logger     logrus.FieldLogger
	loggerHook *test.Hook
}

func (s *ProviderTestSuite) SetupTest() {
	s.logger, s.loggerHook = test.NewNullLogger()

	s.container = cup.NewApp()
	s.container.Set("logger", s.logger)

	s.provider = new(Provider)
}

func (s *ProviderTestSuite) TestRegisterEnabled() {
	s.provider.Register(s.container)

	enabled, err := s.container.Get("push_consumer.enabled")
	s.Require().NoError(err)
	s.Require().Equal(true, enabled)
}

func (s *ProviderTestSuite) TestRegisterDisabled() {
	os.Setenv(enabledEnvName, "false")
	defer os.Unsetenv(enabledEnvName)

	s.provider.Register(s.container)

	enabled, err := s.container.Get("push_consumer.enabled")
	s.Require().NoError(err)
	s.Require().Equal(false, enabled)
}

func TestProviderTestSuite(t *testing.T) {
	suite.Run(t, new(ProviderTestSuite))
}
